import java.sql.*;

public class DBRunner {
  public static void main(String[] args) {
    try {
      // adapt username/password to connect to your DB!!
      Connection cx = DriverManager.getConnection("jdbc:postgresql://localhost:5432/Enterprise",
        "postgres",
        "postgress");
      Statement statement = cx.createStatement();
      System.out.println("Updated " + statement.executeUpdate(
        "UPDATE departments set department_name='Java' where department_id=7") + " row(s)");
      ResultSet rs =statement.executeQuery("select department_id,department_name from departments");
      while (rs.next()){
        if (rs.isFirst()){
          System.out.printf("%3s %-20s\n","ID","name");
          System.out.println("-".repeat(25));
        }
        System.out.printf("%3s %-20s\n",rs.getInt("department_id"),rs.getString("department_name"));
      }
      statement.close();
      cx.close();
      System.out.println("Closing time");
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }
}
